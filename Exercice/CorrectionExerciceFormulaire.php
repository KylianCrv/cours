<!DOCTYPE html>
<html>
    <head>
        <title>NouveautÃ©s html5 - Les formulaires</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="stylesheet" href="../css/template.css">
        <link rel="icon"  href="../images/favicon.ico" >
    </head>
    <body>

        <h4>CrÃ©ation de votre compte utilisateur</h4>


        <form action="formulaire.php" method="post" >

            <table width="600px">
                <tr>
                    <td width="300px"><label for="pseudo">Nom d'utilisateur (*) :</label></td>
                    <td><input required type="text" name="nomutilisateur" id="pseudo"  pattern="[a-zA-Z09_-]{6,}" placeholder="Saisir un nom d'utilisateur" value=""></td>

                </tr>
                <tr>
                    <td><label for="pw">Mot de passe (*) :</label></td>
                    <td><input required type="password" name="motpasse" id="pw"  pattern=".{6,12}" placeholder="De 6 Ã  12 caractÃ¨res" value=""></td>

                </tr>

                <tr>
                    <td><label for="email">Adresse email (*) :</label></td>
                    <td><input type="email" name="email" id="email"  autocomplete="off"  value="" required></td>

                </tr>
                <tr>
                    <td><label for="email2">Confirmez votre email (*)  :</label></td>
                    <td><input type="email" name="email2" id="email2"  autocomplete="off" oninput="verifEmail()" value="" required></td>
                </tr>

            </table>


            <details>
                <summary>
                    Informations complÃ©mentaires :
                </summary>

                <table width="600px">
                    <tr>
                        <td width="300px"><label for="nom">Nom (*) :</label></td>
                        <td><input type="text" name="nom" id="nom" pattern="[a-zA-Z']{1,}" required></td>

                    </tr>
                    <tr>
                        <td><label for="prenom">PrÃ©nom :</label></td>
                        <td><input type="text" name="prenom" id="prenom"pattern="[a-zA-Zâ€™-]{0,}" ></td>

                    </tr>
                    <tr>
                        <td><label for="naissance">Votre date de naissance : </label></td>
                        <td><input type="date" name="birthday" id="naissance" value=""></td>

                    </tr>

                    <tr>
                        <td><label for="tel">TÃ©lÃ©phone (*) :</label></td>
                        <td><input required type="tel" name="telephone" id="tel" pattern="[0-9]{2}.[0-9]{2}.[0-9]{2}.[0-9]{2}.[0-9]{2}" placeholder="Ex 01-02-03-04-05"  value=""></td>

                    </tr>

                    <tr>
                    <datalist id="loisirs">
                        <option value="Musique"/>
                        <option value="CinÃ©ma"/>
                        <option value="ThÃ©Ã¢tre"/>
                        <option value="Sport"/>
                        <option value="Restaurant"/>
                    </datalist>

                    <td><label for="loisirs">Votre loisir prÃ©fÃ©rÃ© : </label></td>
                    <td><input type="text" name="loisirs" id="loisirs" list="loisirs" value="" spellcheck="true"></td>

                    </tr>
                    <tr>
                        <td><label for="couleur">Affecter une couleur Ã  votre compte : </label></td>
                        <td><input type="color" name="couleur" id="couleur"  value="" ></td>

                    </tr>
                    <tr>
                        <td><label for="note">Donner une note au site : </label></td>
                        <td><input type="range" name="note"  id="note" min="0" max="10" step="1" value="0" oninput="document.getElementById('resultat').innerHTML=document.getElementById('note').value" ><strong><span id="resultat">0</span> / 10</strong></td>
                    </tr>
                    <tr>
                        <td colspan="2"><label for="commentaire">Place aux commentaires :</label>
                    </tr>
                    <tr>
                        <td colspan="2"> <textarea name="commentaire" id="commentaire" rows="4" cols="68" ></textarea></td>

                    </tr>

                    <tr>
                        <td> <input type="submit" formnovalidate="formnovalidate" name="sauvegarder" value="Enregistrer et valider plus tard"></td>

                        <td><input type="submit" name="valider" value="Valider"/></td>
                    </tr>

                </table>
            </details>
        </form>


        <hr class="mef">
        <a href="../index.php">Retour au sommaire</a>

        <script>
            function verifEmail() {

                var email2 = document.getElementById('email2');

                if (document.getElementById('email').value != email2.value) {

                    return email2.setCustomValidity('Les deux adresses e-mail doivent Ãªtre identiques');
                }

                email2.setCustomValidity('');
            }
        </script>

    </body>
</html>

