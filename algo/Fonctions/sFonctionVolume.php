<html>
    <head>
        <title>Séraphin Gourmand</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="stylesheet" href="PageCentrale.css">

        <link href="https://fonts.googleapis.com/css?family=Josefin+Slab&display=swap" rel="stylesheet">
    </head>
    <body>
        <?php

        function cylindre($a, $b) {
            $res = M_PI * pow($a, 2) * $b;
            return $res;
        }

        function cone($a, $b) {
            $res = ((1 / 3) * M_PI * pow($a, 2) * $b);
            return $res;
        }

        if (isset($_POST["btn_envoi"])) {
            $hauteur = $_POST["hauteur"];
            $rayon = $_POST["rayon"];
            $forme = $_POST["forme"];
            $volume = cylindre($hauteur, $rayon);
            $volumecone = cone($hauteur, $rayon);
            if ("cylindre" == $_POST["forme"]) {
                printf("le volume du cylindre est de %f cm3", $volume);
            } else {
                printf("le volume du cone est de %f cm3", $volumecone);
            }
        }
        ?>
        <form action="sFonctionVolume.php" method="POST">
            Veuillez saisir la hauteur en cm <input type="number" value="" name="hauteur">
            Veuillez saisir le rayon en cm <input type="number" value="" name="rayon"><br>

            Choisir votre calcul :
            <label for="Cone"><input type="radio" id="cone" name="forme" value="cone">Cone</label>

            <label for="Cylindre"><input type="radio" id="cylindre" name="forme" value="cylindre">Cylindre</label>

            <input type="submit" name="btn_envoi" value="valider">
        </form>
    </body>
</html>