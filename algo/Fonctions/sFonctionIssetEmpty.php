<?php

printf("valeur avec isset : %b , valeur avec empty : %b", isset($a), empty($a));
$a = 16;
printf("<br> pour a = 16, valeur avec isset : %b , valeur avec empty : %b", isset($a), empty($a));
$a = 0;
printf("<br> pour a = 0, valeur avec isset : %b , valeur avec empty : %b", isset($a), empty($a));
$a = null;
printf("<br> pour a = null, valeur avec isset : %b , valeur avec empty : %b", isset($a), empty($a));
$a = 16;
unset($a);
printf("<br> pour a = 16, valeur avec isset : %b , valeur avec empty : %b", isset($a), empty($a));
?>
