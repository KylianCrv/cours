<html>
    <head>
        <title>Gourmandise Séraphin Parys</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="stylesheet" href="PageCentrale.css">
        <link rel="icon" href="../images/logo1.png" />
        <link href="https://fonts.googleapis.com/css?family=Josefin+Slab&display=swap" rel="stylesheet">
        <link rel="stylesheet" href="PageCentraleCSSResponsive.css">
    </head>
    <body>
        <?php
        ?>
        <img class="logo"  src="../images/logo150px.jpg" alt>
        <nav>
            <ul class="menu">
                <li>
                    <a href="#slider"><span>ACCUEIL</span></a>
                </li>
                <li>
                    <a href="#histoire"><span>NOTRE HISTOIRE</span></a>
                </li>
                <li>
                    <a href="#savoirfaire"><span>NOTRE SAVOIR-FAIRE</span></a>
                </li>
                <li>
                    <a href="#produits"><span>NOS PRODUITS</span></a>
                </li>
                <li>
                    <a href="#contact"><span>CONTACT</span></a>
                </li>
            </ul>

        </nav>
        <section id="slider">
            <div >
                <figure>
                    <img src="../images/9029.jpg" alt>
                    <img src="../images/597.jpg" alt>
                    <img src="../images/nature.jpg" alt>
                    <img src="../images/9029.jpg" alt>

                </figure>
            </div>
        </section>
        <section id="histoire">
            <div id="contenuhistoire">
                <h1>L'HISTOIRE DE SERAPHIN PARYS</h1><br>
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam condimentum commodo lorem. Etiam sed pretium lorem. Vivamus enim erat, semper at turpis consequat, faucibus porttitor nisl. Proin iaculis, augue id luctus ornare, metus libero pulvinar mi, et rhoncus nisl enim egestas risus. Phasellus placerat est quis elementum convallis. Maecenas vehicula in sapien sed sollicitudin. Phasellus feugiat, nisl nec dapibus ornare, ligula lorem semper justo, eu tristique ligula massa et nunc. Curabitur eget rutrum purus, vitae accumsan neque. Maecenas tempor et felis id condimentum. Sed neque risus, mollis eu justo ut, congue lobortis lorem. Fusce porta orci enim, eu consequat arcu tincidunt nec.</p>
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam condimentum commodo lorem. Etiam sed pretium lorem. Vivamus enim erat, semper at turpis consequat, faucibus porttitor nisl. Proin iaculis, augue id luctus ornare, metus libero pulvinar mi, et rhoncus nisl enim egestas risus. Phasellus placerat est quis elementum convallis. Maecenas vehicula in sapien sed sollicitudin. Phasellus feugiat, nisl nec dapibus ornare, ligula lorem semper justo, eu tristique ligula massa et nunc. Curabitur eget rutrum purus, vitae accumsan neque. Maecenas tempor et felis id condimentum. Sed neque risus, mollis eu justo ut, congue lobortis lorem. Fusce porta orci enim, eu consequat arcu tincidunt nec.</p>
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam condimentum commodo lorem. Etiam sed pretium lorem. Vivamus enim erat, semper at turpis consequat, faucibus porttitor nisl. Proin iaculis, augue id luctus ornare, metus libero pulvinar mi, et rhoncus nisl enim egestas risus. Phasellus placerat est quis elementum convallis. Maecenas vehicula in sapien sed sollicitudin. Phasellus feugiat, nisl nec dapibus ornare, ligula lorem semper justo, eu tristique ligula massa et nunc. Curabitur eget rutrum purus, vitae accumsan neque. Maecenas tempor et felis id condimentum. Sed neque risus, mollis eu justo ut, congue lobortis lorem. Fusce porta orci enim, eu consequat arcu tincidunt nec.</p>

            </div>
            <div id="imghistoire">
                <img src="../images/demarche.png">
            </div>
        </section>
        <section id="savoirfaire">

            <h1>NOTRE SAVOIR-FAIRE</h1>
            <div id="imgsavoirfaire"><img src="../images/yyyy.jpg"></div>
            <div id="contenusavoirfaire">
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam condimentum commodo lorem. Etiam sed pretium lorem. Vivamus enim erat, semper at turpis consequat, faucibus porttitor nisl. Proin iaculis, augue id luctus ornare, metus libero pulvinar mi, et rhoncus nisl enim egestas risus. Phasellus placerat est quis elementum convallis. Maecenas vehicula in sapien sed sollicitudin. Phasellus feugiat, nisl nec dapibus ornare, ligula lorem semper justo, eu tristique ligula massa et nunc. Curabitur eget rutrum purus, vitae accumsan neque. Maecenas tempor et felis id condimentum. Sed neque risus, mollis eu justo ut, congue lobortis lorem. Fusce porta orci enim, eu consequat arcu tincidunt nec.</p>
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam condimentum commodo lorem. Etiam sed pretium lorem. Vivamus enim erat, semper at turpis consequat, faucibus porttitor nisl. Proin iaculis, augue id luctus ornare, metus libero pulvinar mi, et rhoncus nisl enim egestas risus. Phasellus placerat est quis elementum convallis. Maecenas vehicula in sapien sed sollicitudin. Phasellus feugiat, nisl nec dapibus ornare, ligula lorem semper justo, eu tristique ligula massa et nunc. Curabitur eget rutrum purus, vitae accumsan neque. Maecenas tempor et felis id condimentum. Sed neque risus, mollis eu justo ut, congue lobortis lorem. Fusce porta orci enim, eu consequat arcu tincidunt nec.</p>

            </div>

        </section>
        <section id="produits">
            <div id="titreproduit">
                <h1>NOS PRODUITS</h1>
            </div>
            <div id="produit1">
                <img src="../images/pdts.png"><br>

                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam condimentum commodo lorem. Etiam sed pretium lorem. Vivamus enim erat, semper at turpis consequat, faucibus porttitor nisl. Proin iaculis, augue id luctus ornare, metus libero pulvinar mi, et rhoncus nisl enim egestas risus. Phasellus placerat est quis elementum convallis. Maecenas vehicula in sapien sed sollicitudin. Phasellus feugiat, nisl nec dapibus ornare, ligula lorem semper justo, eu tristique ligula massa et nunc. Curabitur eget rutrum purus, vitae accumsan neque. Maecenas tempor et felis id condimentum. Sed neque risus, mollis eu justo ut, congue lobortis lorem. Fusce porta orci enim, eu consequat arcu tincidunt nec.</p>

            </div>
            <div id="produit2">
                <img src="../images/pdts.png"><br>
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam condimentum commodo lorem. Etiam sed pretium lorem. Vivamus enim erat, semper at turpis consequat, faucibus porttitor nisl. Proin iaculis, augue id luctus ornare, metus libero pulvinar mi, et rhoncus nisl enim egestas risus. Phasellus placerat est quis elementum convallis. Maecenas vehicula in sapien sed sollicitudin. Phasellus feugiat, nisl nec dapibus ornare, ligula lorem semper justo, eu tristique ligula massa et nunc. Curabitur eget rutrum purus, vitae accumsan neque. Maecenas tempor et felis id condimentum. Sed neque risus, mollis eu justo ut, congue lobortis lorem. Fusce porta orci enim, eu consequat arcu tincidunt nec.</p>

            </div>
            <div id="produit3">
                <img src="../images/pdts.png"><br>
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam condimentum commodo lorem. Etiam sed pretium lorem. Vivamus enim erat, semper at turpis consequat, faucibus porttitor nisl. Proin iaculis, augue id luctus ornare, metus libero pulvinar mi, et rhoncus nisl enim egestas risus. Phasellus placerat est quis elementum convallis. Maecenas vehicula in sapien sed sollicitudin. Phasellus feugiat, nisl nec dapibus ornare, ligula lorem semper justo, eu tristique ligula massa et nunc. Curabitur eget rutrum purus, vitae accumsan neque. Maecenas tempor et felis id condimentum. Sed neque risus, mollis eu justo ut, congue lobortis lorem. Fusce porta orci enim, eu consequat arcu tincidunt nec.</p>

            </div>
        </section>
        <section id="contact"><h1 id="h1contact">POUR NOUS CONTACTER</h1>
            <div id="reseauxsociaux">
                <div class="fb-page" data-href="https://www.facebook.com/facebook" data-tabs="" data-width="" data-height="" data-small-header="false" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="true"><blockquote cite="https://www.facebook.com/facebook" class="fb-xfbml-parse-ignore"><a href="https://www.facebook.com/facebook">Facebook</a></blockquote></div>

                <div><a href="https://twitter.com/twitter?ref_src=twsrc%5Etfw" class="twitter-follow-button" data-show-count="false">Follow @GourmandiseSéraphinParys</a><script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script></div>
            </div>
            <div id="formulaire">
                <form method="POST" action="PageCentrale.php"id="form">                   <label for="nom">Nom :</label>
                    <input type="text" name="nom" id="nom" value="" placeholder="Saisissez votre nom" size="30"><br>
                    <label for="email">Email :</label>
                    <input type="email" id="email" name="email" value="" placeholder="Saisissez votre email" size='30'><br>
                    <label for="commentaire">Commentaires:</label>
                    <textarea name="commentaire" id="commentaire" placeholder="Veuillez rédiger votre message" cols="50"rows="12">
                    </textarea><br>
                    <input type="submit" name="btn_envoi" value="Envoyer">
                </form>
            </div>

            <div id="geoloc"><iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d1547.8825526463254!2d4.071500103136348!3d49.272270472495016!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x47e975dff3eda03b%3A0x9c3642a5b6cc6999!2sBoulangerie%20des%20Bois!5e0!3m2!1sfr!2sfr!4v1580761837877!5m2!1sfr!2sfr" width="100%" height="450" frameborder="0" style="border:0;" allowfullscreen=""></iframe></div>
        </section>

        <footer>
            <img id="logofooter" src="../images/logo1.png">
            <ul id="footer1">

                RETROUVEZ NOUS


                <li>
                    <a href="Mentionslegales.php">Où déguster Gourmandise ? </a>
                </li>
                <li>
                    <a href="Mentionslegales.php">Culture chocolat</a>
                </li>
            </ul>

            <ul id="footer2">

                QUI SOMMES-NOUS


                <li>
                    <a href="Mentionslegales.php">Notre culture d'entreprise</a>
                </li>
                <li>
                    <a href="Mentionslegales.php">Notre démarche qualité</a>
                </li>
            </ul>

            <ul id="footer3">

                CONTACTEZ-NOUS


                <li>
                    <a href="Mentionslegales.php">FAQ</a>
                </li>
                <li>
                    <a href="Mentionslegales.php">Recrutement</a>
                </li>
            </ul>
            <ul id="footer4">SERVICE CLIENT
                <li id="tel">DU LUNDI AU JEUDI DE 8H À 18H ET LE VENDREDI DE 8H À 17H</li>

                <li>
                    <a id="btn_message" href="Mentionslegales.php"target="_blank">Nous contacter</a>
                </li>
                <li>
                    Suivez-nous
                </li>
                <li>
                    <a href="https://twitter.com" target="_blank"alt>
                        <img src="../images/logo-twitter-png-rond-3.png">
                    </a>
                    <a href="https://facebook.com"target="_blank"alt>
                        <img src="../images/c5ef04b0aeb3c8d4e929360ec9709602.png">
                    </a>
                    <a href="https://instagram.com"target="_blank">
                        <img src="../images/logo-instagram-noir.png">
                    </a>
                </li>
            </ul>
            <div id="copyright">
                <ul>
                    <li><a href="PlanDuSite.php">Plan du site</a></li>
                    <li><a href="Mentionslegales.php">Informations légales</a></li>
                    <li><a href="Mentionslegales.php">Politique de cookies</a></li>
                    <li><a href="Mentionslegales.php">Conditions générales</a></li>
                    <li>&copy; Kylian Carvalho 2020</li>
            </div>
        </footer>
    </div>
</div>
<div class="site-cache" id="site-cache">
</div>
</div>
</div>
<div id="fb-root">
    <script async defer crossorigin="anonymous" src="https://connect.facebook.net/fr_FR/sdk.js#xfbml=1&version=v6.0"></script></div>

</body>
</html>