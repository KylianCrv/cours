<html>
    <head>
        <title>Plan du site</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="stylesheet" href="PageCentrale.css">
        <link rel="icon" href="../images/logo1.png" />
        <link href="https://fonts.googleapis.com/css?family=Josefin+Slab&display=swap" rel="stylesheet">
        <link rel="stylesheet" href="PageCentraleCSSResponsive.css">
    </head>
    <body id="plandusite">
        <img class="logo"  src="../images/logo150px.jpg" alt>
        <nav>
            <ul class="menu">
                <li>
                    <a href="PageCentrale.php#slider"><span>ACCUEIL</span></a>
                </li>
                <li>
                    <a href="PageCentrale.php#histoire"><span>NOTRE HISTOIRE</span></a>
                </li>
                <li>
                    <a href="PageCentrale.php#savoirfaire"><span>NOTRE SAVOIR-FAIRE</span></a>
                </li>
                <li>
                    <a href="PageCentrale.php#produits"><span>NOS PRODUITS</span></a>
                </li>
                <li>
                    <a href="PageCentrale.php#contact"><span>CONTACT</span></a>
                </li>
            </ul>

        </nav>
        <header>
            <h1>PLAN DU SITE</h1>
        </header>
        <main>
            <h4>PAGES</h4>
            <ul>
                <li>
                    <a href="PageCentrale.php">Page Gourmandise Séraphin Parys</a>
                </li>
                <li>
                    <a href="Mentionslegales.php">Mentions légales</a>
                </li>
                <li>
                    <a href="PlanDuSite.php">Plan du site</a>
                </li>
            </ul>
            <h4>ARTICLES</h4>
            <ul>
                <li>
                    <a href="PageCentrale.php#slider">Galerie de nos produits</a>
                </li>
                <li>
                    <a href="PageCentrale.php#histoire">L'histoire de Séraphin Parys</a>
                </li>
                <li>
                    <a href="PageCentrale.php#savoirfaire">Notre démarche qualité</a>
                </li>
                <li>
                    <a href="PageCentrale.php#produits">Nos produits</a>
                </li>
                <li>
                    <a href="PageCentrale.php#contact">Nous contacter</a>
                </li>
            </ul>
        </main>
        <footer>
            <img id="logofooter" src="../images/logo1.png">
            <ul id="footer1">

                RETROUVEZ NOUS


                <li>
                    <a href="Mentionslegales.php">Où déguster Gourmandise ? </a>
                </li>
                <li>
                    <a href="Mentionslegales.php">Culture chocolat</a>
                </li>
            </ul>

            <ul id="footer2">

                QUI SOMMES-NOUS


                <li>
                    <a href="Mentionslegales.php">Notre culture d'entreprise</a>
                </li>
                <li>
                    <a href="Mentionslegales.php">Notre démarche qualité</a>
                </li>
            </ul>

            <ul id="footer3">

                CONTACTEZ-NOUS


                <li>
                    <a href="Mentionslegales.php">FAQ</a>
                </li>
                <li>
                    <a href="Mentionslegales.php">Recrutement</a>
                </li>
            </ul>
            <ul id="footer4">SERVICE CLIENT
                <li id="tel">DU LUNDI AU JEUDI DE 8H À 18H ET LE VENDREDI DE 8H À 17H</li>

                <li>
                    <a id="btn_message" href="Mentionslegales.php"target="_blank">Nous contacter</a>
                </li>
                <li>
                    Suivez-nous
                </li>
                <li>
                    <a href="https://twitter.com" target="_blank"alt>
                        <img src="../images/logo-twitter-png-rond-3.png">
                    </a>
                    <a href="https://facebook.com"target="_blank"alt>
                        <img src="../images/c5ef04b0aeb3c8d4e929360ec9709602.png">
                    </a>
                    <a href="https://instagram.com"target="_blank">
                        <img src="../images/logo-instagram-noir.png">
                    </a>
                </li>
            </ul>
            <div id="copyright">
                <ul>
                    <li><a href="PlanDuSite.php">Plan du site</a></li>
                    <li><a href="Mentionslegales.php">Informations légales</a></li>
                    <li><a href="Mentionslegales.php">Politique de cookies</a></li>
                    <li><a href="Mentionslegales.php">Conditions générales</a></li>
                    <li>&copy; Kylian Carvalho 2020</li>
            </div>
        </footer>
    </body>
</html>