<html>
    <head>
        <title>Mentions légales</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="stylesheet" href="PageCentrale.css">
        <link rel="icon" href="../images/logo1.png" />
        <link href="https://fonts.googleapis.com/css?family=Josefin+Slab&display=swap" rel="stylesheet">
        <link rel="stylesheet" href="PageCentraleCSSResponsive.css">
    </head>
    <body id="mentionslegales">
        <?php
        ?>


        <img class="logo"  src="../images/logo150px.jpg" alt>
        <nav>
            <ul class="menu">
                <li>
                    <a href="PageCentrale.php#slider"><span>ACCUEIL</span></a>
                </li>
                <li>
                    <a href="PageCentrale.php#histoire"><span>NOTRE HISTOIRE</span></a>
                </li>
                <li>
                    <a href="PageCentrale.php#savoirfaire"><span>NOTRE SAVOIR-FAIRE</span></a>
                </li>
                <li>
                    <a href="PageCentrale.php#produits"><span>NOS PRODUITS</span></a>
                </li>
                <li>
                    <a href="PageCentrale.php#contact"><span>CONTACT</span></a>
                </li>
            </ul>

        </nav>
        <header>
            <h1>MENTIONS LEGALES</h1>
        </header>
        <main>
            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam condimentum commodo lorem. Etiam sed pretium lorem. Vivamus enim erat, semper at turpis consequat, faucibus porttitor nisl. Proin iaculis, augue id luctus ornare, metus libero pulvinar mi, et rhoncus nisl enim egestas risus. Phasellus placerat est quis elementum convallis. Maecenas vehicula in sapien sed sollicitudin. Phasellus feugiat, nisl nec dapibus ornare, ligula lorem semper justo, eu tristique ligula massa et nunc. Curabitur eget rutrum purus, vitae accumsan neque. Maecenas tempor et felis id condimentum. Sed neque risus, mollis eu justo ut, congue lobortis lorem. Fusce porta orci enim, eu consequat arcu tincidunt nec.

            <h5>1. Informations légales</h5>

            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam condimentum commodo lorem. Etiam sed pretium lorem. Vivamus enim erat, semper at turpis consequat, faucibus porttitor nisl. Proin iaculis, augue id luctus ornare, metus libero pulvinar mi, et rhoncus nisl enim egestas risus. Phasellus placerat est quis elementum convallis. Maecenas vehicula in sapien sed sollicitudin. Phasellus feugiat, nisl nec dapibus ornare, ligula lorem semper justo, eu tristique ligula massa et nunc. Curabitur eget rutrum purus, vitae accumsan neque. Maecenas tempor et felis id condimentum. Sed neque risus, mollis eu justo ut, congue lobortis lorem. Fusce porta orci enim, eu consequat arcu tincidunt nec.

            <h5>2. Hébergement du Site :</h5>
            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam condimentum commodo lorem. Etiam sed pretium lorem. Vivamus enim erat, semper at turpis consequat, faucibus porttitor nisl. Proin iaculis, augue id luctus ornare, metus libero pulvinar mi, et rhoncus nisl enim egestas risus. Phasellus placerat est quis elementum convallis. Maecenas vehicula in sapien sed sollicitudin. Phasellus feugiat, nisl nec dapibus ornare, ligula lorem semper justo, eu tristique ligula massa et nunc. Curabitur eget rutrum purus, vitae accumsan neque. Maecenas tempor et felis id condimentum. Sed neque risus, mollis eu justo ut, congue lobortis lorem. Fusce porta orci enim, eu consequat arcu tincidunt nec.

            <h5>3. Droits de propriété intellectuelle</h5>

            Le Site et l’ensemble de son contenu (en particulier les marques commerciales, logos, tout élément graphique, textuel et informatique du Site dont notamment les arborescences, mises en forme, articles, présentations, illustrations, photographies, et/ou tout autre document) sont protégés par les règlementations françaises et internationales relatives à la propriété intellectuelle.

            Tous droits réservés copyright © - depuis Février 2020.

            En aucun cas, la navigation sur le Site, ou les mentions y figurant, ne peuvent être interprétées comme accordant une licence d'utilisation ou un quelconque droit d'utiliser ces éléments.
            A ce titre, leurs représentations, reproductions, imbrications, diffusions et rediffusions, partielles ou totales, sont interdites (article L. 122-4 du Code de la propriété intellectuelle français). Toute personne y procédant sans pouvoir justifier d'une autorisation préalable et expresse du détenteur de ces droits encourt les peines relatives au délit de contrefaçon (articles L. 335-2 et suivants du Code de la propriété intellectuelle français). L’Editeur pourra se prévaloir de ses droits, en particulier de propriété intellectuelle, pour engager des poursuites notamment pénales.

            L’Editeur se réserve le droit d'améliorer ou de modifier à tout moment les informations, produits, programmes ou conditions d’utilisation décrits dans le Site, et ce sans que sa responsabilité puisse être engagée de ce fait. Les conditions et mentions applicables seront celles en vigueur à la date de votre connexion au Site.

            Toutes les marques citées, en particulier Valrhona, sont des marques déposées.

            Les photographies utilisées sur le Site sont protégées par le droit d’auteur. Pour des raisons pratiques, les crédits ne sont pas mentionnés sur la photographie. Pour plus d’information sur l’auteur des photographies, vous pouvez contacter l’Editeur via le formulaire de contact figurant sur le Site.

            <h5>4. Liens hypertextes</h5>

            Le Site peut comporter des liens hypertextes vers des sites tiers. L'Internaute qui serait redirigé vers un site tiers par l'intermédiaire d'un lien hypertexte établi en direction d’autres sites reconnaît que l’Editeur ne maîtrise pas le contenu, les informations, les produits et services offerts sur ces sites. En conséquence, l’Editeur ne pourra en aucun cas voir sa responsabilité engagée pour d’éventuels dommages directs ou indirects résultant de l'utilisation de sites accessibles via les liens hypertextes.
            L’Editeur n’est également pas responsable des liens hypertextes pointant vers le Site.

            Toute personne souhaitant mettre en place des liens hypertextes pointant vers le Site devra en obtenir l’autorisation écrite et préalable de l’Editeur.

            <h5>5. Limitation de responsabilité</h5>

            L’Editeur rappelle aux Internautes les caractéristiques et les limites du réseau internet. L’Editeur ne saurait être tenu responsable de tout dommage résultant de l’accès au Site ou, au contraire, de l’impossibilité d’y accéder. L’Editeur ne pourra en aucun cas être tenu responsable en cas d’indisponibilité du Site, pour quelque cause que ce soit (défaut technique, encombrement du réseau, …).

            L'Internaute admet expressément utiliser le Site à ses propres risques et sous sa responsabilité exclusive. L’Editeur ne saurait être tenu responsable de tout dommage, direct ou indirect, matériel ou immatériel causé aux Internautes, à leurs équipements informatiques ou de télécommunications, aux données qui y sont stockées et aux conséquences pouvant en découler sur leur activité personnelle, professionnelle ou commerciale.

            Les informations du Site sont fournies à titre indicatif. Les informations diffusées sur le Site sont actualisées et vérifiées périodiquement mais peuvent encore contenir des erreurs. Les informations doivent en tout état de cause être prises en considération au moment de leur mise en ligne et non au moment de la consultation du Site (ces données peuvent avoir fait l’objet de mises à jour entre le moment où vous les avez téléchargées et celui où vous en prenez connaissance). En tout état de cause, la responsabilité de l’Editeur ne saurait être engagée du fait de ces informations.

            <h5>6. Accès à des zones protégées par un mot de passe</h5>

            L'accès à des zones protégées par un mot de passe est réservé uniquement aux personnes autorisées. L’Internaute s’engage à ne pas forcer l’accès à ces zones, et notamment à ne pas usurper l’identité, le code d’accès ou l’adresse d’un autre utilisateur. Toute personne non autorisée tentant d'accéder à ces zones pourra faire l'objet de poursuites judiciaires.

            <h5>7. Utilisation du site</h5>

            L’Editeur propose le Site principalement pour l’usage de sa clientèle professionnelle, ses distributeurs, les particuliers et de toute personne souhaitant connaître et acheter ses produits et services.

            Lorsque vous naviguez/vous connectez au Site :

            Vous vous engagez à respecter les présentes mentions ainsi que, plus généralement, les législations applicables,
            Vous vous abstenez de toute collecte, utilisation détournée notamment des informations nominatives auxquelles vous pourriez accéder et, de manière générale, de tout acte susceptible de porter atteinte aux tiers.
            Lorsque vous remplissez un formulaire et que vous fournissez des informations, vous vous engagez à :

            Délivrer des informations réelles, exactes, à jour au moment de leur saisie et notamment à ne pas utiliser de faux noms, qualités ou adresses, ou encore des noms, qualités ou adresses sans y être autorisé,
            Maintenir à jour les données d'inscription en vue de garantir en permanence leur caractère réel et exact,
            Ne pas rendre disponible ou distribuer des informations, des programmes ou des éléments illégaux, répréhensibles ou encore nuisibles (tels que des virus, des logiciels de piratage ou de copie),
            Ne pas diffuser des propos, opinions ou informations à caractère diffamatoire, obscène, violent, raciste et plus généralement contrevenant aux lois et règlements en vigueur, aux droits des personnes et aux bonnes mœurs.
            En cas de violation de ces dispositions, l’Editeur se réserve le droit de vous interdire l'accès au Site ou aux services proposés, à vos torts exclusifs et ce, sans préjudice de toute action en responsabilité.

            <h5>8. Champ d'application / compétence</h5>

            Les présentes conditions s’appliquent à tous les Internautes quels qu’ils soient, sans limitation dans le temps ou dans l’espace.

            SI VOUS ETES COMMERÇANT, VOUS ACCEPTEZ QUE TOUT LITIGE POUVANT DECOULER DE L’UTILISATION DU SITE SOIT EXCLUSIVEMENT DE LA COMPETENCE DE LA LOI FRANÇAISE ET DES TRIBUNAUX COMPETENTS DE PARIS.
        </main>

        <footer>
            <img id="logofooter" src="../images/logo1.png">
            <ul id="footer1">

                RETROUVEZ NOUS


                <li>
                    <a href="Mentionslegales.php">Où déguster Gourmandise ? </a>
                </li>
                <li>
                    <a href="Mentionslegales.php">Culture chocolat</a>
                </li>
            </ul>

            <ul id="footer2">

                QUI SOMMES-NOUS


                <li>
                    <a href="Mentionslegales.php">Notre culture d'entreprise</a>
                </li>
                <li>
                    <a href="Mentionslegales.php">Notre démarche qualité</a>
                </li>
            </ul>

            <ul id="footer3">

                CONTACTEZ-NOUS


                <li>
                    <a href="Mentionslegales.php">FAQ</a>
                </li>
                <li>
                    <a href="Mentionslegales.php">Recrutement</a>
                </li>
            </ul>
            <ul id="footer4">SERVICE CLIENT
                <li id="tel">DU LUNDI AU JEUDI DE 8H À 18H ET LE VENDREDI DE 8H À 17H</li>

                <li>
                    <a id="btn_message" href="Mentionslegales.php"target="_blank">Nous contacter</a>
                </li>
                <li>
                    Suivez-nous
                </li>
                <li>
                    <a href="https://twitter.com" target="_blank"alt>
                        <img src="../images/logo-twitter-png-rond-3.png">
                    </a>
                    <a href="https://facebook.com"target="_blank"alt>
                        <img src="../images/c5ef04b0aeb3c8d4e929360ec9709602.png">
                    </a>
                    <a href="https://instagram.com"target="_blank">
                        <img src="../images/logo-instagram-noir.png">
                    </a>
                </li>
            </ul>
            <div id="copyright">
                <ul>
                    <li><a href="PlanDuSite.php">Plan du site</a></li>
                    <li><a href="Mentionslegales.php">Informations légales</a></li>
                    <li><a href="Mentionslegales.php">Politique de cookies</a></li>
                    <li><a href="Mentionslegales.php">Conditions générales</a></li>
                    <li>&copy; Kylian Carvalho 2020</li>
            </div>
        </footer>
    </body>
</html>
