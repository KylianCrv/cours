<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
        <?php
        $msg = "";
        if (isset($_POST["valider"])) {
            echo "Je vais traiter la facture du client n° :" . $_POST["id_client"];
        }
        $msg = "";
        if (isset($_POST["btn_txt"])) {
            echo $_POST["f_textarea"];
        }
        $msg = "";
        if (isset($_POST["btn_fruit"])) {
            $msg = implode(",", $_POST["sel_fruit"]);
            echo "Votre fruit préféré est :<strong>" . $msg . "</strong><br>";
        }
        $msg = "";
        if (isset($_POST["btn_couleur"])) {
            $msg = $_POST["sel_couleur"];
            echo "Votre couleur préférée est :<strong>" . $msg . "</strong><br>";
        }
        $msg = "";
        if (isset($_POST["btn_list"])) {
            $msg = $_POST["f_couleurs"];
            echo "Votre couleur préférée est :<strong>" . $msg . "</strong><br>";
        }
        ?>
        <!-- Zone de type select multiple-->
        <form action ="sFormulaire.php" method ="POST">
            <p>Une zone de type select multiple</p>
            <p>Votre fruit préférée ?
                <select name="sel_fruit[]"size="5"multiple="multiple">
                    <option value="pomme">Pomme</option>
                    <option value="orange">Orange</option>
                    <option value="poire">Poire</option>
                    <option value="clémentine">Clémentine</option>
                    <option value="cerise">Cerise</option>
                </select>
                <input type="submit"name="btn_fruit"value="Choisir">
            </p>
        </form>
        <!-- zone de type select-->
        <form action ="sFormulaire.php" method ="POST">
            <p>Une zone de type select </p>
            <p>Votre couleur préférée ?
                <select name="sel_couleur">
                    <option value="Choisir">Choisir</option>
                    <option value="orange">Orange</option>
                    <option value="Jaune">Jaune</option>
                    <option value="vert">vert</option>
                    <option value="cerise">Cerise</option>
                </select>
                <input type="submit"name="btn_couleur"value="Choisir">
            </p>
        </form>
        <!-- zone de type textaréa-->
        <form action ="sFormulaire.php" method ="POST">
            Une zone de type textarea<br>
            Pour des commentaires/des observations/des textes longs. <br>
            <textarea name="f_textarea" rows="4" cols="20">Contenu</textarea>
            <input type="submit"name="btn_txt"value="Choisir">
        </form>
        <!-- Zone de type cachée -->
        <form action ="sFormulaire.php" method ="POST">
            Une zone de type cachée<br>
            <input type="hidden" name="id_client"value="37">
            <input type="submit"name="valider"value="valider">
        </form>
        <!--Nouveaux champs-->
        <meter name="stock"value="10"min="0"max="20">10</meter>
        <datalist id="couleurs">
            <option value="bleu">
            <option value="vert">
            <option value="gris">
        </datalist>
        <form action="sFormulaire.php" method="POST">
            Couleur des yeux <br>
            <input type="text"name="f_couleurs"list="couleurs">
            <input type="submit"name="btn_list"value="Choisir">
        </form>
        <!-- balise output -->
        <form action="sFormulaire.php" method="POST" oninput="tva.value=(t.value-(t.value)/(x.value))">
            Montant TTC :
            <input type="text" name="t"value="">
            € <br>
            Taux de TVA : 5,5%
            <input type="hidden" name="x"value="1.055"><br>
            Le montant de TVA déductible est :
            <output id="tva" name="tva"value="0">
            </output>€<br>
            <input type="date"min="2020-07-01"max="2020-08-31"value=""><br>
            <input type="time"min="14:00"max="18:00"name=""step="900"><br>
            <input type="range"min="-3.06"max="7.88"step="0.2"name="f_range"><br>
            <input type="color"name="f_color"><br>
            Nom <sup>(*)</sup> : <input type="text"name=""placeholder="Veuillez saisir votre texte ici..."name="f_nom"required="required"value=""><br>
            identifiant : <input type="text"name="f_id"pattern="[A-F]{2}[0-9]{4}"required="required"placeholder="2 Lettres et 4 chiffres"value=""><br>
        </form>
        <canvas
</body>
</html>